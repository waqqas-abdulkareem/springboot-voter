#Magic. Reads env variables from .env file
#See: https://unix.stackexchange.com/questions/235223/makefile-include-env-file
include .env
export $(shell sed 's/=.*//' .env)

compile:
	./gradlew -Pversion=$(VERSION) build

docker-local: compile
	docker-compose -f docker-compose.local.yml up --build

docker-production: compile
	docker-compose -f docker-compose.production.yml up --build

docker-hub-publish: compile
	docker-compose -f docker-compose.production.yml build
	docker-compose -f docker-compose.production.yml push webapp

heroku-publish: docker-hub-publish
    heroku container:login
    docker tag wkas/$(NAME):$(VERSION) registry.heroku.com/$(NAME)/app
    docker push registry.heroku.com/$(NAME)/app

docker-db-shell:
	docker exec -t -i mysql /bin/bash

docker-clean:
	docker image prune
	docker container prune

mysqldump:
	echo "Dumping Table structures"
	mysqldump -u root -p --no-data voter > data/schema.sql
	echo "Dumping data of selected tables"
	mysqldump -u root -p voter roles >> data/schema.sql