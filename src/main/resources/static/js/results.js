function pollResults(params){

    var csrf = params.csrf;
    var headers = {};

    headers[csrf.header] = csrf.token;

    var poll = params.poll;
    $.ajax({
        url: "/"+poll.owner.id+"/polls/"+poll.slug+"/votes",
        method: "GET",
        headers: headers
    }).done(function(res){
        drawChart(res,params);
    }).fail(function(err){
        alert(err);
    });
}

function drawChart(res,params){

    var getBackgroundColor = function(userSelectionId,option){
        return userSelectionId == option.id ? 'rgba(255, 206, 86, 0.2)': 'rgba(54, 162, 235, 0.2)';
    }

    var getBorderColor = function(userSelectionId,option){
        return userSelectionId == option.id ? 'rgba(255, 206, 86, 1)': 'rgba(54, 162, 235, 1)';
    }

    var getTitle = function(userSelectionId, option){
        var title = option.title;
        if(userSelectionId == option.id){
            title = title + ' ' + params.i18n.yourVote;
        }
        return title;
    }

    var userSelectionId = params.poll.userSelection;
    var ctx = document.getElementById("chart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: res.options.map(x => getTitle(userSelectionId,x)),
            datasets: [{
                label: params.i18n.label,
                data: res.options.map(x => x.votes),
                backgroundColor: res.options.map(x => getBackgroundColor(userSelectionId,x)),
                borderColor: res.options.map(x => getBorderColor(userSelectionId,x)),
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        stepSize: 1,
                        beginAtZero:true
                    }
                }]
            },
            title:{
                display: true,
                text: params.i18n.totalVotes + ' : '+res.totalVotes,
                position: 'bottom'
            }
        }
    });
}