function addOptionButton(maxOptions){

    var optionItemInputs = document.getElementsByClassName("option-item");
    var nextItemIndex = optionItemInputs.length;
    var parentElement = optionItemInputs[0].parentElement;

    var newOptionItem = document.createElement("input");
    newOptionItem.setAttribute('type', "text");
    newOptionItem.setAttribute('name', 'options['+nextItemIndex+']');
    newOptionItem.classList.add("form-control");
    newOptionItem.classList.add("option-item");

    parentElement.appendChild(newOptionItem);

    if(nextItemIndex == (maxOptions - 1)){
        document.getElementById("add_option_button").remove();
    }
}