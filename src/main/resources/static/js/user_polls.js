function confirmDeletePolls(pollOwnerId,csrfHeader,csrfToken){
    var hasSelection =  $("[name='postId']:checked").length > 0
    if(hasSelection && confirm("Are you sure you want to delete these polls?")){
        deletePolls(pollOwnerId,csrfHeader,csrfToken);
    }
}

function deletePolls(pollOwnerId,csrfHeader, csrfToken){

    var params = [];
    $("[name='postId']:checked").each(function(){
        params.push($(this).attr("data-poll-id"));
    });

    var headers = {};
    headers[csrfHeader] = csrfToken;

    $.ajax({
        url: "/"+pollOwnerId+"/polls/delete",
        method: "POST",
        data: JSON.stringify(params),
        contentType: 'application/json; charset=UTF-8',
        headers: headers
    }).done(function(res){
        if(res.deletedCount > 0){
            location.reload(true)
        }
    }).fail(function(err){
        alert(err);
    });
}