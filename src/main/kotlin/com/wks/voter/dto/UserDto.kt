package com.wks.voter.dto

import com.wks.voter.domain.User
import org.hibernate.validator.constraints.Length
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

data class UserDto(

        @get: Email(message = "{email.invalid}")
        @get: NotEmpty(message = "{email.required}")
        var email: String = "",

        @get: Length(min = User.MIN_LENGTH_PASSWORD, max = User.MAX_LENGTH_PASSWORD, message = "{password.length}")
        var password: String = "",

        @get: NotEmpty(message = "{first_name.required}")
        var firstName: String = "",

        @get: NotEmpty(message = "{last_name.required}")
        var lastName: String = ""

)