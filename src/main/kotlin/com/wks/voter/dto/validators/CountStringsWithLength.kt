package com.wks.voter.dto.validators

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

/**
 * Validates that there are between {@link minCount} and {@link maxCount} number of strings
 * having a length between {@link minLength} and {@link maxLength}
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [StringCollectionSizeValidator::class])
annotation class CountStringsWithLength(
        val message: String = "collection must contain one string",
        val minCount: Int = 1,//min count of strings
        val maxCount: Int = -1,//max count of strings
        val minLength: Int = -1,//min length of string
        val maxLength: Int = -1,//max length of collection
        val groups: Array<KClass<Any>> = [],
        val payload: Array<KClass<Payload>> = []
        )