package com.wks.voter.dto.validators

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

/**
 * Validates that there are between minCount and maxCount number of strings that have a length between minLength and maxLength
 */
class StringCollectionSizeValidator : ConstraintValidator<CountStringsWithLength, Collection<String>> {

    private lateinit var constraint: CountStringsWithLength

    override fun initialize(constraintAnnotation: CountStringsWithLength?) {
        super.initialize(constraintAnnotation)

        this.constraint = constraintAnnotation!!
    }

    override fun isValid(value: Collection<String>?, context: ConstraintValidatorContext?): Boolean {
        val validateStringLength = { test: String ->
            var valid = true
            if (constraint.minLength >= 0) {
                valid = valid and (test.length >= constraint.minLength)
            }
            if (constraint.maxLength >= 0) {
                valid = valid and (test.length <= constraint.maxLength)
            }
            valid
        }

        val countValidStrings = value?.filter { validateStringLength(it) }?.size ?: 0
        return countValidStrings >= constraint.minCount && countValidStrings <= constraint.maxCount
    }
}