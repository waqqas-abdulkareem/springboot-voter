package com.wks.voter.dto

import com.wks.voter.domain.Option
import com.wks.voter.domain.Poll
import com.wks.voter.dto.validators.CountStringsWithLength
import org.hibernate.validator.constraints.Length

data class PollDto(

        @get: Length(
                min = Poll.MIN_LENGTH_TITLE,
                max = Poll.MAX_LENGTH_TITLE,
                message = "{poll.title.length}"
        )
        var title: String = "",

        @get: CountStringsWithLength(
                minCount = Poll.MIN_COUNT_OPTIONS,
                maxCount = Poll.MAX_COUNT_OPTIONS,
                minLength = Option.MIN_LENGTH_TITLE,
                maxLength = Option.MAX_LENGTH_TITLE,
                message = "{poll.options.count}"
        )
        var options: List<String> = List(Poll.MIN_COUNT_OPTIONS) { "" }
) {

    override fun toString(): String {
        return "title: ${title}, options: ${options}"
    }
}