package com.wks.voter.dto

import com.wks.voter.domain.Option
import com.wks.voter.domain.Poll

data class OptionIdVotesDto(
        val optionId: Long,
        val votes: Long
)

class OptionVotesDto(option: Option,
                     val votes: Long) {

    val id = option.id
    val title = option.title
}

class PollResultsDto(poll: Poll,
                     rawResults: List<OptionIdVotesDto>) {

    val id = poll.id
    val title = poll.title
    val slug = poll.slug
    val options: List<OptionVotesDto>
    val totalVotes: Long

    init {
        val votesMap = rawResults.map { it.optionId to it.votes }.toMap()
        options = poll.options.map { OptionVotesDto(it, votesMap[it.id] ?: 0) }
        totalVotes = options.map { it.votes }.sum()
    }
}