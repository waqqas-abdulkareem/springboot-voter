package com.wks.voter.dto

data class DeletedPollsDto(val deletedCount: Int)