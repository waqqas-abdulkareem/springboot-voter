package com.wks.voter.dto

import com.wks.voter.domain.Poll

data class PollPathDto(val poll: Poll,
                       val path: String,
                       val userPath: String)