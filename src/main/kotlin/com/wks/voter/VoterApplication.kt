package com.wks.voter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class VoterApplication

fun main(args: Array<String>) {
    runApplication<VoterApplication>(*args)
}
