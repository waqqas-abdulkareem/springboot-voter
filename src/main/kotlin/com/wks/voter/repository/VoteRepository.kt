package com.wks.voter.repository

import com.wks.voter.domain.Poll
import com.wks.voter.domain.User
import com.wks.voter.domain.Vote
import com.wks.voter.dto.OptionIdVotesDto
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Transactional
@Repository
interface VoteRepository : JpaRepository<Vote, Long> {
    fun findByUserAndPoll(user: User, poll: Poll): Vote?

    @Query("SELECT new com.wks.voter.dto.OptionIdVotesDto(v.option.id,COUNT(v.option.id)) " +
            "FROM com.wks.voter.domain.Vote v " +
            "WHERE v.poll.id=:pollId " +
            "GROUP BY v.option.id")
    fun pollResults(@Param("pollId") pollId: Long): List<OptionIdVotesDto>
}