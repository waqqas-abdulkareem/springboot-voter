package com.wks.voter.repository

import com.wks.voter.domain.Poll
import com.wks.voter.domain.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Transactional
@Repository
interface PollRepository : JpaRepository<Poll, Long> {

    fun findByUserId(userId: Long): List<Poll>

    fun findByUserIdAndSlug(userId: Long, slug: String): Poll?

    fun countByUser(user: User): Long

    @Modifying
    @Transactional
    @Query
    fun deleteByUserAndIdIn(user: User, ids: List<Long>): Int

    fun countByUserAndIdIn(user: User, ids: List<Long>): Int
}