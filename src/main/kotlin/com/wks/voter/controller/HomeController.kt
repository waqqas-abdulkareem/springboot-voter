package com.wks.voter.controller

import com.wks.voter.dto.PollPathDto
import com.wks.voter.service.polls.PollService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class HomeController @Autowired constructor(private val pollService: PollService) {

    @GetMapping("/")
    fun index(): ModelAndView {

        val recentPolls = pollService.recentPolls();
        val recentPollPaths = recentPolls.map {
            PollPathDto(it,"/${it.user!!.id}/polls/${it.slug}/vote","/${it.user.id}/polls")
        }

        val modelAndView = ModelAndView("index")
        modelAndView.addObject("pollPaths",recentPollPaths)

        return modelAndView
    }

}