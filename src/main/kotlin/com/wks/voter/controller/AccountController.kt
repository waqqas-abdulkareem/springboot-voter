package com.wks.voter.controller

import com.wks.voter.domain.User
import com.wks.voter.dto.UserDto
import com.wks.voter.service.avatar.AvatarNotFoundException
import com.wks.voter.service.avatar.AvatarService
import com.wks.voter.service.storage.EmptyFileException
import com.wks.voter.service.storage.StorageException
import com.wks.voter.service.storage.UnsupportedContentTypeException
import com.wks.voter.service.user.UserService
import com.wks.voter.service.user.UsernameExistsException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@Controller
class AccountController @Autowired constructor(val userService: UserService,
                                               val avatarService: AvatarService,
                                               val messageSource: MessageSource) {

    companion object {
        val LOGGER = LoggerFactory.getLogger(AccountController::class.java)!!
    }

    @GetMapping("/login")
    fun login(auth: Authentication?): String = if (auth?.isAuthenticated == true) {
        "redirect:/"
    } else {
        "login"
    }

    @GetMapping("/register")
    fun register(auth: Authentication?): ModelAndView = if (auth?.isAuthenticated == true) {
        ModelAndView("redirect:/")
    } else {
        ModelAndView("register", "user", UserDto())
    }

    @PostMapping("/register")
    fun completeRegistration(@ModelAttribute("user") @Valid userDto: UserDto,
                             result: BindingResult,
                             request: HttpServletRequest): ModelAndView {

        if (result.hasErrors()) {
            return ModelAndView("register", "user", userDto)
        }

        try {
            userService.saveUser(userDto = userDto)
        } catch (u: UsernameExistsException) {
            result.rejectValue("email", "username.exists", "This username is no longer available")
            return ModelAndView("register", "user", userDto)
        }

        request.setAttribute("new_account", true)
        return ModelAndView("redirect:/login")
    }

    @GetMapping("/avatar")
    fun avatar() = "avatar"

    @GetMapping("/profile/avatar",produces = ["image/jpeg","image/jpg"])
    @ResponseBody
    fun userAvatar(auth: Authentication) : ResponseEntity<ByteArray>{

        val avatarPath = (auth.principal as User).avatarPath
                ?: throw AvatarNotFoundException("")

        val attachment = avatarService.loadResource(avatarPath)
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"${attachment.fileName!!}\"")
                .contentType(attachment.mediaType)
                .contentLength(attachment.contentLength)
                .body(attachment.byteArray)
    }


    @PostMapping("/avatar")
    fun uploadAvatar(file: MultipartFile,
                     auth: Authentication,
                     redirectAttributes: RedirectAttributes,
                     locale: Locale): ModelAndView {

        val modelAndView = ModelAndView("avatar")

        var user = auth.principal as User
        try {
            user = user.copy(avatarPath = avatarPath(file,user))

            avatarService.store(file, user.avatarPath!!)
            userService.updateUser(user)

            val message = messageSource.getMessage("account.avatar.upload.success", null, locale)
            modelAndView.addObject("message", message)
            modelAndView.addObject("uploadSuccess", true)

        } catch (s: StorageException) {

            LOGGER.error(s.message, s)

            val message = when (s) {
                is EmptyFileException -> messageSource.getMessage(
                        "account.avatar.upload.empty_file",
                        null,
                        locale
                )
                is UnsupportedContentTypeException -> messageSource.getMessage(
                        "account.avatar.unsupported_type",
                        arrayOf(s.supportedTypes.joinToString(",")),
                        locale
                )
                else -> messageSource.getMessage(
                        "account.avatar.upload.failure",
                        null,
                        locale
                )
            }

            modelAndView.addObject("message", message)
            modelAndView.addObject("uploadSuccess", false)
        }

        return modelAndView
    }

    private fun avatarPath(file: MultipartFile, user: User) : String{
        val extension = file.originalFilename!!.split(".").last()
        val fileName = user.id.toString()

        return "$fileName.$extension"
    }

    @ExceptionHandler(AvatarNotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleAvatarNotFoundException(t : Throwable,
                                      locale: Locale) : ModelAndView {

        val message = messageSource.getMessage("account.avatar.not_found",null,locale)

        val modelAndView = ModelAndView("avatar")
        modelAndView.addObject("uploadSuccess",false)
        modelAndView.addObject("message",message)

        return modelAndView
    }
}