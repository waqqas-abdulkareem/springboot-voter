package com.wks.voter.controller

import com.wks.voter.domain.Poll
import com.wks.voter.domain.Role.Companion.USER
import com.wks.voter.domain.User
import com.wks.voter.dto.DeletedPollsDto
import com.wks.voter.dto.PollDto
import com.wks.voter.dto.PollPathDto
import com.wks.voter.service.polls.PollService
import com.wks.voter.service.user.UserNotFoundException
import com.wks.voter.service.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import javax.validation.Valid

@Controller
class PollController @Autowired constructor(private val userService: UserService,
                                            private val pollService: PollService) {

    @PreAuthorize("hasAuthority('$USER')")
    @GetMapping("/poll")
    fun pollForm(): ModelAndView {

        val modelAndView = ModelAndView("new_poll")
        modelAndView.addObject("poll", PollDto())
        modelAndView.addObject("maxOptions", Poll.MAX_COUNT_OPTIONS);
        return modelAndView
    }

    @PreAuthorize("hasAuthority('$USER')")
    @PostMapping("/poll")
    fun createPoll(@ModelAttribute("poll") @Valid pollDto: PollDto,
                   bindingResult: BindingResult,
                   auth: Authentication): ModelAndView {

        val modelAndView = ModelAndView()
        modelAndView.viewName = "new_poll"
        modelAndView.addObject("poll", pollDto)

        if (bindingResult.hasErrors()) {
            return modelAndView
        }

        val poll: Poll?
        try {
            val user = auth.principal as User
            poll = pollService.savePoll(pollDto, user)
        } catch (e: DataIntegrityViolationException) {
            bindingResult.rejectValue(
                    "title",
                    "poll.slug.duplicate",
                    "A poll with a similar title already exists"
            )
            return modelAndView
        }

        val pollPath = PollPathDto(
                poll = poll,
                path = "/${poll.user!!.id}/polls/${poll.slug}/results",
                userPath =  "/${poll.user.id}/polls"
        )

        modelAndView.viewName = "poll_created"
        modelAndView.addObject("pollPath", pollPath)

        return modelAndView
    }

    @GetMapping("/{pollOwnerId:\\d+}/polls")
    fun listPolls(@PathVariable("pollOwnerId") pollOwnerId: Long,
                  auth: Authentication?): ModelAndView {

        val pollPaths = pollService.pollsForUserId(pollOwnerId)
                .map { PollPathDto(it, "/$pollOwnerId/polls/${it.slug}/vote","/$pollOwnerId/polls") }

        val pollOwner = if (pollPaths.isEmpty()) {
            userService.findUserById(pollOwnerId) ?: throw UserNotFoundException(pollOwnerId)
        } else {
            pollPaths.first().poll.user!!
        }

        val user = auth?.principal as User?
        val pollOwnerIsUser = pollOwner.username == user?.email

        val modelAndView = ModelAndView("user_polls")
        modelAndView.addObject("pollPaths", pollPaths)
        modelAndView.addObject("pollOwner", pollOwner)
        modelAndView.addObject("pollOwnerIsUser", pollOwnerIsUser)

        return modelAndView
    }

    @PreAuthorize("hasAuthority('$USER')")
    @PostMapping(
            value = ["/{pollOwnerId:\\d+}/polls/delete"],
            consumes = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
    @ResponseBody
    fun deletePolls(@RequestBody pollIds: List<Long>,
                    auth: Authentication): DeletedPollsDto {
        //Future Improvement: Separate deletion into two requests: one to generate delete token, the second to actually delete

        val user = auth.principal as User

        val deletedCount = pollService.deletePollsById(user, pollIds)

        return DeletedPollsDto(deletedCount = deletedCount)
    }
}