package com.wks.voter.controller

import com.wks.voter.domain.User
import com.wks.voter.exceptions.IllegalOperationException
import com.wks.voter.service.options.OptionNotFoundException
import com.wks.voter.service.polls.PollNotFoundException
import com.wks.voter.service.user.UserNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.multipart.MaxUploadSizeExceededException
import org.springframework.web.servlet.ModelAndView
import java.util.*
import javax.servlet.http.HttpServletRequest

@ControllerAdvice
class ExceptionControllerAdvice @Autowired constructor(val messageSource: MessageSource,
                                                       @Value("\${spring.servlet.multipart.max-file-size}") val maxUploadSize: String) {

    private val LOGGER = LoggerFactory.getLogger(ExceptionControllerAdvice::class.java)

    @ExceptionHandler(Throwable::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun handleException(throwable: Throwable,
                        request: HttpServletRequest,
                        locale: Locale): ModelAndView {

        LOGGER.error("Exception while processing request ${request.method} ${request.requestURI}. Locale: $locale", throwable)

        val modelAndView = ModelAndView("error")
        val title = messageSource.getMessage("error.title.internal_server_error", null, locale)
        val message = throwable.message ?: messageSource.getMessage("error.message.internal_server_error", null, locale)

        modelAndView.addObject("title", title)
        modelAndView.addObject("message", message)

        return modelAndView
    }

    @ExceptionHandler(AccessDeniedException::class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    fun accessDenied(auth: Authentication?,
                     throwable: Throwable,
                     request: HttpServletRequest,
                     locale: Locale): ModelAndView {

        val user = auth?.principal as User?
        LOGGER.error("Access Denied ${request.method} ${request.requestURI}. Locale: $locale. User: ${user?.email}", throwable)

        val modelAndView = ModelAndView("error")
        val title = messageSource.getMessage("error.title.access_denied", null, locale)

        modelAndView.addObject("title", title)

        return modelAndView
    }

    @ExceptionHandler(MaxUploadSizeExceededException::class)
    @ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
    fun uploadExceptionHandler(t : Throwable,
                               locale: Locale): ModelAndView {

        val title = messageSource.getMessage("error.title.illegal_operation", null, locale)
        val message = messageSource.getMessage("error.upload.size_limit_exceeded", arrayOf(maxUploadSize), locale)

        val modelAndView = ModelAndView("error")
        modelAndView.addObject("title", title)
        modelAndView.addObject("message", message)

        return modelAndView
    }

    @ExceptionHandler(value = [
        UserNotFoundException::class,
        PollNotFoundException::class,
        OptionNotFoundException::class
    ])
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun notFoundExceptions(exception: Exception,
                           locale: Locale): ModelAndView {

        val messageKey = when (exception) {
            is UserNotFoundException -> "error.not_found.user"
            is PollNotFoundException -> "error.not_found.poll"
            is OptionNotFoundException -> "error.not_found.option"
            else -> "Not found"
        }

        val modelAndView = ModelAndView("error")
        val title = messageSource.getMessage("error.title.not_found", null, locale)
        val message = messageSource.getMessage(messageKey, null, locale)

        modelAndView.addObject("message", message)
        modelAndView.addObject("title", title)

        return modelAndView
    }

    @ExceptionHandler(IllegalOperationException.ModifyingUnownedData::class)
    fun modifyingUnownedData(exception: Exception,
                             request: HttpServletRequest,
                             locale: Locale): ModelAndView {

        LOGGER.error("${request.pathInfo}: $exception", exception)

        request.logout()

        val title = messageSource.getMessage("error.title.illegal_operation", null, locale)
        val message = messageSource.getMessage("error.polls.not_owner", null, locale)

        val modelAndView = ModelAndView("error")
        modelAndView.addObject("title", title)
        modelAndView.addObject("message", message)

        return modelAndView
    }
}