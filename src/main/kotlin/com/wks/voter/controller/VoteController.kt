package com.wks.voter.controller

import com.wks.voter.domain.Poll
import com.wks.voter.domain.Role
import com.wks.voter.domain.User
import com.wks.voter.domain.Vote
import com.wks.voter.dto.PollResultsDto
import com.wks.voter.service.options.OptionNotFoundException
import com.wks.voter.service.polls.PollNotFoundException
import com.wks.voter.service.polls.PollService
import com.wks.voter.service.votes.VoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes

@Controller
class VoteController @Autowired constructor(private val pollService: PollService,
                                            private val voteService: VoteService) {

    @PreAuthorize("hasAuthority('${Role.USER}')")
    @GetMapping("/{pollOwnerId:\\d+}/polls/{slug}/vote")
    fun votingForm(@PathVariable("pollOwnerId") pollOwnerId: Long,
                   @PathVariable("slug") slug: String,
                   redirectAttributes: RedirectAttributes,
                   auth: Authentication): ModelAndView {

        //TODO: Fix repetition in GET and POST
        val poll = pollService.pollForUserIdWithSlug(pollOwnerId, slug)!! //?: throw NotFoundException.Poll(slug)
        val pollOwner = poll.user
        val user = auth.principal as User
        val existingVote = voteService.voteByUserOnPoll(user, poll)

        val userIsOwner = user == pollOwner
        val userAlreadyVoted = existingVote != null

        if (userIsOwner || userAlreadyVoted) {
            redirectAttributes.addFlashAttribute("existingVote",existingVote)
            redirectAttributes.addFlashAttribute("poll",poll)
            return ModelAndView("redirect:/$pollOwnerId/polls/$slug/results")
        }

        return ModelAndView("vote", "poll", poll)
    }

    @PreAuthorize("hasAuthority('${Role.USER}')")
    @PostMapping("/{pollOwnerId:\\d+}/polls/{slug}/vote")
    fun voteOnPoll(@PathVariable("pollOwnerId") pollOwnerId: Long,
                   @PathVariable("slug") slug: String,
                   @RequestParam(value = "option", required = true) optionId: Long,
                   redirectAttributes: RedirectAttributes,
                   auth: Authentication): ModelAndView {
        //TODO: Fix repetition in GET and POST
        val poll = pollService.pollForUserIdWithSlug(pollOwnerId, slug) ?: throw PollNotFoundException(slug)
        val pollOwner = poll.user
        val user = auth.principal as User
        val existingVote = voteService.voteByUserOnPoll(user, poll)

        val userIsOwner = user == pollOwner
        val userAlreadyVoted = existingVote != null

        if (userIsOwner || userAlreadyVoted) {
            redirectAttributes.addFlashAttribute("existingVote",existingVote)
            redirectAttributes.addFlashAttribute("poll",poll)
            return ModelAndView("redirect:/$pollOwnerId/polls/$slug/results")
        }

        // I wanted to make optionMap a lazy property in `Poll` but there was a problem with
        // Hibernate and @Transient.
        val optionMap = poll.options.map { it.id to it }.toMap()
        val option = optionMap[optionId] ?: throw OptionNotFoundException(optionId)

        voteService.saveVote(Vote(user, poll, option))

        return ModelAndView("redirect:/$pollOwnerId/polls/$slug/results")
    }

    @PreAuthorize("hasAuthority('${Role.USER}')")
    @GetMapping("/{pollOwnerId:\\d+}/polls/{slug}/results")
    fun viewResult(@PathVariable("pollOwnerId") pollOwnerId: Long,
                   @PathVariable("slug") slug: String,
                   @ModelAttribute("poll") redirectPoll : Poll?,
                   redirectPollErrors : BindingResult,
                   @ModelAttribute("existingVote") redirectVote: Vote?,
                   redirectExistingVoteErrors : BindingResult,
                   auth: Authentication): ModelAndView {

        val user = auth.principal as User

        val poll = redirectPoll
                ?: pollService.pollForUserIdWithSlug(pollOwnerId, slug)
                ?: throw PollNotFoundException(slug)

        val existingVote = redirectVote
                ?: voteService.voteByUserOnPoll(user, poll)

        val modelAndView = ModelAndView("results")
        modelAndView.addObject("poll", poll)
        existingVote?.let { modelAndView.addObject("existingVote", existingVote) }
        return modelAndView
    }

    @ResponseBody
    @PreAuthorize("hasAuthority('${Role.USER}')")
    @GetMapping(
            value = ["/{pollOwnerId:\\d+}/polls/{slug}/votes"],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
    fun votingResults(@PathVariable("pollOwnerId") pollOwnerId: Long,
                      @PathVariable("slug") slug: String): PollResultsDto {

        val poll = pollService.pollForUserIdWithSlug(pollOwnerId, slug) ?: throw PollNotFoundException(slug)
        return voteService.pollResults(poll)

    }
}