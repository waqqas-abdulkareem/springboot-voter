package com.wks.voter.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
@Table(name = "options")
data class Option(

        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(name = "title", nullable = false)
        @get: Length(min = MIN_LENGTH_TITLE, max = MAX_LENGTH_TITLE)
        val title: String,

        @JsonIgnore
        @ManyToOne
        @JoinColumn(name = "poll_id", nullable = false)
        val poll: Poll?
) {

    companion object {
        const val MIN_LENGTH_TITLE = 1
        const val MAX_LENGTH_TITLE = 255
    }

    private constructor() : this(
            id = 0,
            title = "",
            poll = null
    )
}