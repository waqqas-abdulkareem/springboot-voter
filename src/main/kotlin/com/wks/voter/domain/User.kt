package com.wks.voter.domain

import org.hibernate.validator.constraints.Length
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "users")
data class User(@Id
                @Column(name = "id", unique = true)
                @GeneratedValue(strategy = GenerationType.IDENTITY)
                val id: Long,

                @Column(name = "email", unique = true, nullable = false)
                @get: Email(message = "email_invalid")
                @get: NotEmpty(message = "email_required")
                val email: String,

                @get:JvmName("getPassword__java()")//ugly
                @Column(name = "password", nullable = false)
                @get: Length(min = MIN_LENGTH_PASSWORD, max = MAX_LENGTH_PASSWORD, message = "password_min_length")
                val password: String,

                @Column(name = "first_name", nullable = false)
                @get: NotEmpty(message = "first_name_required")
                val firstName: String,

                @Column(name = "last_name", nullable = false)
                @get: NotEmpty(message = "last_name_required")
                val lastName: String,

                @Column(name="avatar_path",nullable = true)
                val avatarPath: String? = null,

                @Column(name = "account_expired", nullable = false)
                val accountExpired: Boolean = false,

                @Column(name = "credentials_expired", nullable = false)
                val credentialsExpired: Boolean = false,

                @Column(name = "account_enabled", nullable = false)
                val accountEnabled: Boolean = false,

                @ManyToMany(fetch = FetchType.EAGER)
                @JoinTable(
                        name = "user_roles",
                        joinColumns = [JoinColumn(name = "user_id")],
                        inverseJoinColumns = [JoinColumn(name = "role_id")]
                )
                val roles: Set<Role>) : UserDetails {

    companion object {
        const val MIN_LENGTH_PASSWORD = 6
        const val MAX_LENGTH_PASSWORD = 255
    }

    private constructor() :
            this(
                    id = 0,
                    email = "",
                    password = "",
                    firstName = "",
                    lastName = "",
                    avatarPath = null,
                    accountExpired = false,
                    credentialsExpired = false,
                    accountEnabled = false,
                    roles = emptySet<Role>()
            )

    fun fullName() = "$firstName $lastName"

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = roles.toMutableList()

    override fun isEnabled(): Boolean = accountEnabled

    override fun getUsername(): String = email

    override fun getPassword(): String = password

    override fun isCredentialsNonExpired(): Boolean = !credentialsExpired

    override fun isAccountNonExpired(): Boolean = !accountExpired

    override fun isAccountNonLocked(): Boolean = accountEnabled

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is User -> other.id == id
            else -> false
        }
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}