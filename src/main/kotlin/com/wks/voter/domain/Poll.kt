package com.wks.voter.domain

import org.hibernate.validator.constraints.Length
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.Size

@Entity
@EntityListeners(AuditingEntityListener::class)
@Table(name = "polls")
public data class Poll(

        @Id
        @Column(name = "id", unique = true)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(name = "title", nullable = false)
        @get: Length(min = MIN_LENGTH_TITLE, max = MAX_LENGTH_TITLE)
        val title: String,

        @Column(name = "slug", nullable = false)
        @get: Length(min = MIN_LENGTH_SLUG, max = MAX_LENGTH_SLUG)
        val slug: String,

        @Column(name = "enabled", nullable = false)
        val enabled: Boolean = true,

        @CreatedDate
        @Column(name = "created_date", nullable = false, updatable = false)
        val createdDate: LocalDateTime = LocalDateTime.now(),

        @LastModifiedDate
        @Column(name = "modified_date")
        val modifiedDate: LocalDateTime? = null,

        @OneToMany(mappedBy = "poll")
        @get: Size(min = MIN_COUNT_OPTIONS, max = MAX_COUNT_OPTIONS)
        val options: List<Option>,

        @ManyToOne
        @JoinColumn(name = "user_id", nullable = false)
        val user: User?) {

    companion object {
        const val MIN_LENGTH_TITLE = 10
        const val MAX_LENGTH_TITLE = 255

        const val MIN_LENGTH_SLUG = 10
        const val MAX_LENGTH_SLUG = 255

        const val MIN_COUNT_OPTIONS = 2
        const val MAX_COUNT_OPTIONS = 5
    }

    private constructor() :
            this(
                    id = 0,
                    title = "",
                    slug = "",
                    enabled = true,
                    options = emptyList<Option>(),
                    user = null
            )

    override fun toString(): String {
        return "Poll(id=$id, title='$title', slug='$slug')"
    }


}