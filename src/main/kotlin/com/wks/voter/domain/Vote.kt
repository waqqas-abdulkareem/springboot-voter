package com.wks.voter.domain

import com.wks.voter.domain.Vote.Companion.TABLE_VOTES
import javax.persistence.*

@Entity
@Table(name = TABLE_VOTES)
data class Vote(

        @Id
        @Column(name = "id", unique = true)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @ManyToOne
        @JoinColumn(name = "user_id", nullable = false)
        val user: User?,

        @ManyToOne
        @JoinColumn(name = "poll_id", nullable = false)
        val poll: Poll?,

        @OneToOne
        @JoinColumn(name = "option_id", nullable = false)
        val option: Option?
) {
    companion object {
        const val TABLE_VOTES = "votes"
    }

    private constructor() : this(
            id = 0,
            user = null,
            poll = null,
            option = null
    )

    constructor(user: User,
                poll: Poll,
                option: Option) : this(
            0,
            user,
            poll,
            option
    )
}