package com.wks.voter.domain

import org.springframework.security.core.GrantedAuthority
import javax.persistence.*

@Entity
@Table(name = "roles")
data class Role(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "id")
        val id: Long,

        @Column(name = "name", unique = true, nullable = false)
        val name: String
) : GrantedAuthority {

    companion object {
        const val USER = "USER"
        const val ADMIN = "ADMIN"
    }

    private constructor() :
            this(
                    id = 0,
                    name = ""
            )

    override fun getAuthority(): String = name
}