package com.wks.voter.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

sealed class IllegalOperationException {
    @ResponseStatus(HttpStatus.FORBIDDEN)
    data class ModifyingUnownedData(val perpetratorId: Long? = null,
                                    val victimId: Long? = null) : RuntimeException()
}