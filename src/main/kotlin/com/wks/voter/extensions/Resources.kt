package com.wks.voter.extensions

import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import javax.activation.FileTypeMap

fun Resource.mediaType() : MediaType =
    MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(file))