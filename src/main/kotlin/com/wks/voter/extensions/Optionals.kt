package com.wks.voter.extensions

import java.util.*

fun <T> Optional<T>.unwrap(): T? = orElse(null)