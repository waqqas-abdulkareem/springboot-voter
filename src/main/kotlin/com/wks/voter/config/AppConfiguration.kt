package com.wks.voter.config

import org.springframework.boot.web.servlet.ServletContextInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Configuration
open class AppConfiguration {
    @Bean
    open fun passwordEncoder(): BCryptPasswordEncoder = BCryptPasswordEncoder()

    @Bean
    open fun servletContextInitializer() : ServletContextInitializer{
        return ServletContextInitializer { it.sessionCookieConfig.name = "sid" }
    }
}