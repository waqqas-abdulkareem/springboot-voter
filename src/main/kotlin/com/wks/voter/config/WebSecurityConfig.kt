package com.wks.voter.config

import com.wks.voter.service.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import javax.sql.DataSource



@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
open class WebSecurityConfig @Autowired constructor(private val userDetailsService: UserService,
                                                    private val passwordEncoder: BCryptPasswordEncoder,
                                                    private val dataSource: DataSource) : WebSecurityConfigurerAdapter() {


    @Value("\${spring.queries.users-by-username}")
    private val usersByUsernameQuery: String? = null

    @Value("\${spring.queries.authorities-by-username}")
    private val authoritiesByUsernameQuery: String? = null

    override fun configure(http: HttpSecurity?) {
        http?.authorizeRequests()
                ?.requestMatchers(EndpointRequest.toAnyEndpoint())?.hasAuthority("ADMIN")
                ?.antMatchers("/css/**")?.permitAll()
                ?.antMatchers("/js/**")?.permitAll()
                ?.antMatchers("/login")?.permitAll()
                ?.antMatchers("/register")?.permitAll()
                ?.antMatchers("/{\\d+}/polls")?.permitAll()
                ?.antMatchers("/")?.permitAll()
                ?.anyRequest()?.authenticated()
                ?.and()
                ?.formLogin()
                ?.loginPage("/login")
                ?.defaultSuccessUrl("/")
                ?.failureUrl("/login")
                ?.and()
                ?.logout()?.logoutRequestMatcher(AntPathRequestMatcher("/logout"))
                ?.logoutSuccessUrl("/")
                ?.permitAll()
    }

    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth
                ?.userDetailsService(userDetailsService)
                ?.and()
                ?.jdbcAuthentication()
                ?.usersByUsernameQuery(usersByUsernameQuery)
                ?.authoritiesByUsernameQuery(authoritiesByUsernameQuery)
                ?.dataSource(dataSource)
                ?.passwordEncoder(passwordEncoder)
    }
}