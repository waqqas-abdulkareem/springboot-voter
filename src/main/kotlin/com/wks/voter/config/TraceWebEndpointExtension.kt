package com.wks.voter.config

import org.springframework.boot.actuate.endpoint.annotation.ReadOperation
import org.springframework.boot.actuate.endpoint.web.annotation.EndpointWebExtension
import org.springframework.boot.actuate.trace.http.HttpTrace
import org.springframework.boot.actuate.trace.http.HttpTraceEndpoint
import org.springframework.boot.actuate.trace.http.HttpTraceRepository
import org.springframework.stereotype.Component

@Component
@EndpointWebExtension(endpoint = HttpTraceEndpoint::class)
class TraceWebEndpointExtension(val repository : HttpTraceRepository) {

    val EXCLUDE_PATHS = listOf("/js","/css","/actuator")

    @ReadOperation
    fun traces(): HttpTraceDescriptor {
        val traces = repository.findAll().filter { trace -> !isInExclusionList(trace.request) }
        return HttpTraceDescriptor(traces)
    }

    private fun isInExclusionList(request: HttpTrace.Request) : Boolean{
        for (path in EXCLUDE_PATHS){
            if (request.uri.path.startsWith(path))
                return true
        }
        return false
    }

    data class HttpTraceDescriptor(val traces : List<HttpTrace>)
}