package com.wks.voter.service.avatar

import com.wks.voter.service.storage.LoadFileException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus


@ResponseStatus(HttpStatus.NOT_FOUND)
class AvatarNotFoundException(
        fileName: String,
        cause: Throwable? = null,
        message: String? = null
) : LoadFileException(fileName,cause,message)