package com.wks.voter.service.avatar

import com.wks.voter.service.storage.EmptyFileException
import com.wks.voter.service.storage.StorageService
import com.wks.voter.service.storage.StorageService.Attachment
import com.wks.voter.service.storage.UnsupportedContentTypeException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File

@Service
class DefaultAvatarService @Autowired constructor(@Value("\${app.storage.fs.avatars.dir}") val avatarsDirName: String,
                                                  @Value("\${app.avatar.content-types}") formats: String,
                                                  private val storageService: StorageService) : AvatarService {

    companion object {
        val DEFAULT_CREATE_OPTIONS = StorageService.CreateOptions(
                createDirsIfNotExists = true,
                deleteFileIfExists = true
        )
    }

    private val avatarContentTypes: List<String> = formats.split(",")


    override fun store(file: MultipartFile, fileName: String) {

        if (!avatarContentTypes.contains(file.contentType))
            throw UnsupportedContentTypeException(fileName, (file.contentType ?: "null"), avatarContentTypes)

        if (file.isEmpty) throw EmptyFileException(fileName)

        storageService.store(file, actualFileName(fileName), DEFAULT_CREATE_OPTIONS)
    }

    override fun loadResource(fileName: String): Attachment = storageService.loadResource(actualFileName(fileName))

    private fun actualFileName(fileName: String): String = "$avatarsDirName${File.separator}$fileName"
}