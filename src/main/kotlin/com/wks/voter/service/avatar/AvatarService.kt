package com.wks.voter.service.avatar

import com.wks.voter.service.storage.StorageService.Attachment
import com.wks.voter.service.storage.StoreFileException
import org.springframework.web.multipart.MultipartFile

interface AvatarService {
    @Throws(StoreFileException::class)
    fun store(file: MultipartFile, fileName: String)

    @Throws(AvatarNotFoundException::class)
    fun loadResource(fileName: String): Attachment
}