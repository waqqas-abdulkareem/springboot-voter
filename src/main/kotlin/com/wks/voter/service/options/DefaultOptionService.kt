package com.wks.voter.service.options

import com.wks.voter.domain.Option
import com.wks.voter.extensions.unwrap
import com.wks.voter.repository.OptionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultOptionService @Autowired constructor(private val optionRepository: OptionRepository) : OptionService {

    override fun optionById(id: Long): Option? = optionRepository.findById(id).unwrap()
}