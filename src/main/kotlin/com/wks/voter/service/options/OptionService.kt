package com.wks.voter.service.options

import com.wks.voter.domain.Option

interface OptionService {
    fun optionById(id: Long): Option?
}