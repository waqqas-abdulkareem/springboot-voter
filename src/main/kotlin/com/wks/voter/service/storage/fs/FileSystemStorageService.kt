package com.wks.voter.service.storage.fs

import com.wks.voter.extensions.mediaType
import com.wks.voter.service.storage.LoadFileException
import com.wks.voter.service.storage.StorageService
import com.wks.voter.service.storage.StorageService.Attachment
import com.wks.voter.service.storage.StoreFileException
import org.aspectj.weaver.tools.cache.SimpleCacheFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.FileNotFoundException
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

@Service
@Profile("local")
class FileSystemStorageService @Autowired constructor(@Value("\${app.storage.fs.root.dir}") val rootDirectory: String) : StorageService {

    private val root: Path = Paths.get(rootDirectory)

    override fun store(file: MultipartFile, fileName: String, options: StorageService.CreateOptions?) {
        try {

            val resolvedPath = resolvedPath(fileName)

            if (!Files.exists(resolvedPath) && options?.createDirsIfNotExists == true) {
                Files.createDirectories(resolvedPath)
            }

            if (options?.deleteFileIfExists == true) {
                Files.deleteIfExists(resolvedPath)
            }

            Files.copy(file.inputStream, resolvedPath)

        } catch (io: IOException) {
            throw StoreFileException(
                    fileName = SimpleCacheFactory.path,
                    cause = io
            )
        }
    }

    override fun loadResource(fileName: String): Attachment {
        try {
            val resource = resolvedPath(fileName)
                    .let { UrlResource(it.toUri()) }
                    .takeIf { it.exists() || it.isReadable }
                    ?: throw FileNotFoundException(fileName)

            return Attachment(
                    resource.filename,
                    resource.contentLength(),
                    resource.mediaType(),
                    Files.readAllBytes(resource.file.toPath())
            )

        } catch (e: Exception) {
            throw LoadFileException(fileName, e, e.message)
        }
    }

    private fun resolvedPath(fileName: String): Path = root.resolve(fileName)

}