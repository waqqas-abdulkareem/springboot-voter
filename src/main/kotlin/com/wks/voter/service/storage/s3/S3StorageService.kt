package com.wks.voter.service.storage.s3

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.wks.voter.service.storage.LoadFileException
import com.wks.voter.service.storage.StorageService
import com.wks.voter.service.storage.StorageService.Attachment
import com.wks.voter.service.storage.StoreFileException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.StreamUtils
import org.springframework.web.multipart.MultipartFile

@Service
@Profile("production")
class S3StorageService @Autowired constructor(private val s3: AmazonS3,
                                              @Value("\${app.storage.aws.avatar.bucket}") private val bucketName: String) : StorageService {

    override fun store(file: MultipartFile, fileName: String, options: StorageService.CreateOptions?) {
        val metadata = ObjectMetadata()
        metadata.contentLength = file.size
        metadata.contentType = file.contentType
        metadata.contentDisposition = "attachment"

        val putRequest = PutObjectRequest(bucketName, fileName, file.inputStream, metadata)
        try {
            s3.putObject(putRequest)
        } catch (e: Exception) {
            throw StoreFileException(fileName, e)
        }
    }

    override fun loadResource(fileName: String): Attachment {

        try {
            val s3Object = s3.getObject(bucketName, fileName)
            val metadata = s3Object.objectMetadata

            return Attachment(
                    fileName,
                    metadata.contentLength,
                    MediaType.valueOf(metadata.contentType),
                    s3Object.objectContent.use { StreamUtils.copyToByteArray(it) }
            )
        } catch (e: Exception) {
            throw LoadFileException(fileName, e)
        }
    }
}