package com.wks.voter.service.storage

import org.springframework.http.MediaType
import org.springframework.web.multipart.MultipartFile

interface StorageService {
    @Throws(StoreFileException::class)
    fun store(file: MultipartFile, fileName: String, options: CreateOptions? = null)

    @Throws(LoadFileException::class)
    fun loadResource(fileName: String): Attachment

    data class CreateOptions(val createDirsIfNotExists: Boolean = false,
                             val deleteFileIfExists: Boolean = false)

    data class Attachment(val fileName: String,
                          val contentLength: Long,
                          val mediaType: MediaType,
                          val byteArray: ByteArray)
}