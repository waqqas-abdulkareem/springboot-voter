package com.wks.voter.service.storage


open class StorageException(
        val fileName: String,
        cause: Throwable? = null,
        message: String? = cause?.message
) : Exception(message, cause)

class UnsupportedContentTypeException(
        fileName: String,
        val unsupportedType : String,
        val supportedTypes : List<String>,
        cause : Throwable? = null,
        message: String? = cause?.message
) : StorageException(fileName,cause,message)

open class StoreFileException(
        fileName: String,
        cause: Throwable? = null,
        message: String? = cause?.message
) : StorageException(fileName, cause, message)

class EmptyFileException(
        fileName: String
) : StoreFileException(fileName)

open class LoadFileException(
        fileName: String,
        cause: Throwable? = null,
        message: String? = cause?.message
) : StorageException(fileName, cause, message)