package com.wks.voter.service.polls

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
data class PollNotFoundException(val slug: String) : RuntimeException()