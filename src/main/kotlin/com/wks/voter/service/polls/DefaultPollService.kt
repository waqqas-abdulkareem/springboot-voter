package com.wks.voter.service.polls

import com.wks.voter.domain.Option
import com.wks.voter.domain.Poll
import com.wks.voter.domain.User
import com.wks.voter.dto.PollDto
import com.wks.voter.exceptions.IllegalOperationException
import com.wks.voter.repository.OptionRepository
import com.wks.voter.repository.PollRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class DefaultPollService @Autowired constructor(private val pollRepository: PollRepository,
                                                private val optionRepository: OptionRepository) : PollService {

    override fun recentPolls(): List<Poll> {

        val pageRequest = PageRequest.of(0, 20, Sort(Sort.Direction.DESC, "createdDate"))

        return pollRepository.findAll(pageRequest).toList()
    }

    override fun savePoll(pollDto: PollDto, user: User): Poll {

        val options = pollDto.options
                .filter { !it.isEmpty() }
                .map { Option(id = 0L, title = it, poll = null) }

        val poll = Poll(
                id = 0,
                title = pollDto.title,
                slug = slugFromTitle(pollDto.title),
                enabled = true,
                options = options.toMutableList(),
                user = user,
                modifiedDate = LocalDateTime.now()
        )

        val savedPoll = pollRepository.save(poll)

        savedPoll.options
                .map { Option(id = it.id, title = it.title, poll = poll) }
                .forEach { optionRepository.save(it) }

        return savedPoll
    }

    override fun slugFromTitle(title: String): String = title.toLowerCase().replace("[^A-Za-z0-9]".toRegex()) { "_" }

    override fun pollsForUserId(userId: Long): List<Poll> = pollRepository.findByUserId(userId)

    override fun pollForUserIdWithSlug(userId: Long, slug: String): Poll? = pollRepository.findByUserIdAndSlug(userId, slug)

    override fun pollCountForUser(user: User): Long = pollRepository.countByUser(user)

    override fun userHasPolls(user: User): Boolean = pollCountForUser(user) > 0

    override fun deletePollsById(user: User, ids: List<Long>): Int {

        if (!isUserOwnerOfPolls(user, ids)) {
            throw IllegalOperationException.ModifyingUnownedData(perpetratorId = user.id)
        }

        return pollRepository.deleteByUserAndIdIn(user, ids)
    }

    override fun isUserOwnerOfPolls(user: User, pollIds: List<Long>): Boolean = pollRepository.countByUserAndIdIn(user, pollIds) == pollIds.count()
}