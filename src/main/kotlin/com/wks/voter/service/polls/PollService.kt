package com.wks.voter.service.polls

import com.wks.voter.domain.Poll
import com.wks.voter.domain.User
import com.wks.voter.dto.PollDto

interface PollService {
    fun recentPolls(): List<Poll>
    fun savePoll(pollDto: PollDto, user: User): Poll
    fun slugFromTitle(title: String): String
    fun pollsForUserId(userId: Long): List<Poll>
    fun pollForUserIdWithSlug(userId: Long, slug: String): Poll?
    fun userHasPolls(user: User): Boolean
    fun pollCountForUser(user: User): Long
    fun deletePollsById(user: User, ids: List<Long>): Int
    fun isUserOwnerOfPolls(user: User, pollIds: List<Long>): Boolean
}