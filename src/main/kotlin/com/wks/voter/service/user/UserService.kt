package com.wks.voter.service.user

import com.wks.voter.domain.User
import com.wks.voter.dto.UserDto
import org.springframework.security.core.userdetails.UserDetailsService

interface UserService : UserDetailsService {
    fun findUserById(userId: Long): User?
    fun saveUser(userDto: UserDto)
    fun updateUser(user: User) : User
}