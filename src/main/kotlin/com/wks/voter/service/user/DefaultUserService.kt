package com.wks.voter.service.user

import com.wks.voter.domain.Role
import com.wks.voter.domain.User
import com.wks.voter.dto.UserDto
import com.wks.voter.extensions.unwrap
import com.wks.voter.repository.RoleRepository
import com.wks.voter.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class DefaultUserService @Autowired constructor(private val userRepository: UserRepository,
                                                private val roleRepository: RoleRepository,
                                                private val passwordEncoder: BCryptPasswordEncoder) : UserService {

    override fun loadUserByUsername(email: String): UserDetails = userRepository.findByEmail(email)
            ?: throw UsernameNotFoundException(email)

    override fun findUserById(userId: Long): User? = userRepository.findById(userId).unwrap()

    @Throws(
            UsernameExistsException::class
    )
    override fun saveUser(userDto: UserDto) {

        val userRole = roleRepository.findByName(Role.USER)!!
        val password = passwordEncoder.encode(userDto.password)

        val user = User(
                id = 0,
                email = userDto.email,
                password = password,
                firstName = userDto.firstName,
                lastName = userDto.lastName,
                accountEnabled = true,
                accountExpired = false,
                credentialsExpired = false,
                roles = setOf(userRole)
        )

        try {
            userRepository.save(user)
        } catch (e: DataIntegrityViolationException) {
            throw UsernameExistsException(userDto.email)
        }
    }

    override fun updateUser(user: User) : User = userRepository.save(user)
}