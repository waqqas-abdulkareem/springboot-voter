package com.wks.voter.service.user

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
data class UsernameExistsException(val username: String) : Exception()

@ResponseStatus(HttpStatus.NOT_FOUND)
data class UserNotFoundException(val userId: Long) : RuntimeException()