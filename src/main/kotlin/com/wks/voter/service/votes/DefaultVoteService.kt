package com.wks.voter.service.votes

import com.wks.voter.domain.Poll
import com.wks.voter.domain.User
import com.wks.voter.domain.Vote
import com.wks.voter.dto.PollResultsDto
import com.wks.voter.repository.VoteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DefaultVoteService @Autowired constructor(private val voteRepository: VoteRepository) : VoteService {

    override fun voteByUserOnPoll(user: User, poll: Poll): Vote? = voteRepository.findByUserAndPoll(user, poll)

    override fun saveVote(vote: Vote): Vote? = voteRepository.save(vote)

    override fun pollResults(poll: Poll): PollResultsDto = PollResultsDto(poll, voteRepository.pollResults(poll.id))

}