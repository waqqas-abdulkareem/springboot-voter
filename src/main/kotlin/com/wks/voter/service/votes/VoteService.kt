package com.wks.voter.service.votes

import com.wks.voter.domain.Poll
import com.wks.voter.domain.User
import com.wks.voter.domain.Vote
import com.wks.voter.dto.PollResultsDto

interface VoteService {
    fun voteByUserOnPoll(user: User, poll: Poll): Vote?
    fun saveVote(vote: Vote): Vote?
    fun pollResults(poll: Poll): PollResultsDto
}