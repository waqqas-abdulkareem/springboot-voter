package com.wks.voter.domain

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import javax.validation.Validator

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class PollTests {

    @Autowired
    lateinit var validator: Validator
    val validTitle = "A".repeat(Poll.MIN_LENGTH_TITLE)
    val validSlug = "A".repeat(Poll.MAX_LENGTH_SLUG)
    val validOptionTitle = "A".repeat(Option.MIN_LENGTH_TITLE)
    val validOption = Option(id = 0, title = validOptionTitle, poll = null)
    val validOptions = MutableList(Poll.MIN_COUNT_OPTIONS) { validOption }

    @Test
    fun testTitleTooShort() {
        val title = "A".repeat(Poll.MIN_LENGTH_TITLE - 1)
        val poll = Poll(id = 0, title = title, slug = validSlug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "title")
    }

    @Test
    fun testTitleTooLong() {
        val title = "A".repeat(Poll.MAX_LENGTH_TITLE + 1)
        val poll = Poll(id = 0, title = title, slug = validSlug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "title")
    }

    @Test
    fun testTitleMinLength() {
        val title = "A".repeat(Poll.MIN_LENGTH_TITLE)
        val poll = Poll(id = 0, title = title, slug = validSlug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTitleMaxLength() {
        val title = "A".repeat(Poll.MAX_LENGTH_TITLE)
        val poll = Poll(id = 0, title = title, slug = validSlug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testSlugTooShort() {
        val slug = "A".repeat(Poll.MIN_LENGTH_SLUG - 1)
        val poll = Poll(id = 0, title = validTitle, slug = slug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "slug")
    }

    @Test
    fun testSlugTooLong() {
        val slug = "A".repeat(Poll.MAX_LENGTH_SLUG + 1)
        val poll = Poll(id = 0, title = validTitle, slug = slug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "slug")
    }

    @Test
    fun testSlugMinLength() {
        val slug = "A".repeat(Poll.MIN_LENGTH_SLUG)
        val poll = Poll(id = 0, title = validTitle, slug = slug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testSlugMaxLength() {
        val slug = "A".repeat(Poll.MAX_LENGTH_SLUG)
        val poll = Poll(id = 0, title = validTitle, slug = slug, options = validOptions, user = null)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTooFewOptions(){
        val options = MutableList(Poll.MIN_COUNT_OPTIONS - 1) { validOption }
        val poll = Poll(id = 0, title = validTitle, slug = validSlug, options = options, user = null)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "options")
    }

    @Test
    fun testMinOptions(){
        val options = MutableList(Poll.MIN_COUNT_OPTIONS) { validOption }
        val poll = Poll(id = 0, title = validTitle, slug = validSlug, options = options, user = null)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTooManyOptions(){
        val options = MutableList(Poll.MAX_COUNT_OPTIONS + 1) { validOption }
        val poll = Poll(id = 0, title = validTitle, slug = validSlug, options = options, user = null)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "options")
    }

    @Test
    fun testMaxOptions(){
        val options = MutableList(Poll.MAX_COUNT_OPTIONS) { validOption }
        val poll = Poll(id = 0, title = validTitle, slug = validSlug, options = options, user = null)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }
}