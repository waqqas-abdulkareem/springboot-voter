package com.wks.voter.domain

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import javax.validation.Validator

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class OptionTests {

    @Autowired
    lateinit var validator: Validator

    @Test
    fun testTitleTooShort() {
        val title = "A".repeat(Option.MIN_LENGTH_TITLE - 1)
        val option = Option(id = 1L, title = title, poll = null)

        val violations = validator.validate(option)

        assertEquals(violations.first().propertyPath.toString(), "title")
    }

    @Test
    fun testTitleMinLength() {
        val title = "A".repeat(Option.MIN_LENGTH_TITLE)
        val option = Option(id = 1L, title = title, poll = null)

        val violations = validator.validate(option)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTitleTooLong() {
        val title = "A".repeat(Option.MAX_LENGTH_TITLE + 1)
        val option = Option(id = 1L, title = title, poll = null)

        val violations = validator.validate(option)

        assertEquals(violations.first().propertyPath.toString(), "title")
    }

    @Test
    fun testTitleMaxLength() {
        val title = "A".repeat(Option.MAX_LENGTH_TITLE)
        val option = Option(id = 1L, title = title, poll = null)

        val violations = validator.validate(option)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testValidTitle() {
        val title = "A".repeat(Option.MIN_LENGTH_TITLE + 2)
        val option = Option(id = 1L, title = title, poll = null)

        val violations = validator.validate(option)

        assertTrue(violations.isEmpty())
    }
}