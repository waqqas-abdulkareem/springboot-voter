package com.wks.voter.domain

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import javax.validation.Validator

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class UserTests {

    @Autowired
    lateinit var validator: Validator
    val validPassword = "A".repeat(User.MIN_LENGTH_PASSWORD)

    @Test
    fun testEmailEmpty() {
        val user = User(id = 0, email = "", password = validPassword, firstName = "John", lastName = "Smith", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertEquals(violations.first().propertyPath.toString(), "email")
    }

    @Test
    fun testEmailInvalid() {
        val user = User(id = 0, email = "test.com", password = validPassword, firstName = "John", lastName = "Smith", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertEquals(violations.first().propertyPath.toString(), "email")
    }

    @Test
    fun testPasswordTooShort() {
        val password = "A".repeat(User.MIN_LENGTH_PASSWORD - 1)
        val user = User(id = 0, email = "test@test.com", password = password, firstName = "John", lastName = "Smith", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertEquals(violations.first().propertyPath.toString(), "password__java()")
    }

    @Test
    fun testPasswordMinLength() {
        val password = "A".repeat(User.MIN_LENGTH_PASSWORD)
        val user = User(id = 0, email = "test@test.com", password = password, firstName = "John", lastName = "Smith", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testPasswordTooLong() {
        val password = "A".repeat(User.MAX_LENGTH_PASSWORD + 1)
        val user = User(id = 0, email = "test@test.com", password = password, firstName = "John", lastName = "Smith", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertEquals(violations.first().propertyPath.toString(), "password__java()")
    }

    @Test
    fun testPasswordMaxLength() {
        val password = "A".repeat(User.MAX_LENGTH_PASSWORD)
        val user = User(id = 0, email = "test@test.com", password = password, firstName = "John", lastName = "Smith", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testFirstNameEmpty() {
        val user = User(id = 0, email = "test@test.com", password = validPassword, firstName = "", lastName = "Smith", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertEquals(violations.first().propertyPath.toString(), "firstName")
    }

    @Test
    fun testLastNameEmpty() {
        val user = User(id = 0, email = "test@test.com", password = validPassword, firstName = "John", lastName = "", roles = setOf<Role>())

        val violations = validator.validate(user)

        assertEquals(violations.first().propertyPath.toString(), "lastName")
    }
}