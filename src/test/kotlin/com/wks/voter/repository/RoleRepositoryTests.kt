package com.wks.voter.repository

import com.wks.voter.domain.Role
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@DataJpaTest
class RoleRepositoryTests {

    @Autowired
    private lateinit var entityManager: TestEntityManager
    @Autowired
    private lateinit var roleRepository: RoleRepository

    @Test
    fun testFindByName() {
        val userRole = "User"
        //Given
        val role = Role(id = 0, name = userRole)
        entityManager.persist(role)
        entityManager.flush()

        //When
        val result = roleRepository.findByName(userRole)!!
        assertEquals(userRole,result.name)
    }

    @Test
    fun testDuplicateName(){
        val userRole = "User"

        //Given
        val role = Role(id = 0, name = userRole)

        //When
        entityManager.persist(role)
        entityManager.persist(role)
        entityManager.flush()

        //Then
        assertEquals(1,roleRepository.count())
    }
}