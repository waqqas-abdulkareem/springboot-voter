package com.wks.voter.repository

import com.wks.voter.domain.Option
import com.wks.voter.domain.Poll
import com.wks.voter.domain.Role
import com.wks.voter.domain.User
import org.junit.Assert.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager

//@RunWith(SpringRunner::class)
//@ActiveProfiles(profiles = ["local"])
//@DataJpaTest
class PollRepositoryTests {

    @Autowired
    private lateinit var entityManager: TestEntityManager

    @Autowired
    private lateinit var pollRepository: PollRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    //@Test
    fun testFindByUserId() {

        //When
        entityManager.persist(createPoll(id = 1, user = createUser(id = 1)))
        entityManager.persist(createPoll(id = 2, user = createUser(id = 2)))
        entityManager.flush()

        //Then
        val results = pollRepository.findByUserId(1)

        assertEquals(1, results.size)
        assertEquals(1, results.first().id)
    }

    //@Test
    fun findByUserIdAndSlug() {
        //Given
        val firstTitle = "A".repeat(Poll.MIN_LENGTH_TITLE)
        val secondTitle = "B".repeat(Poll.MIN_LENGTH_TITLE)

        //When
        entityManager.persist(createPoll(id = 1, title = firstTitle))
        entityManager.persist(createPoll(id = 2, title = secondTitle))
        entityManager.flush()

        //Then
        val poll = pollRepository.findByUserIdAndSlug(2, secondTitle)!!

        assertEquals(secondTitle, poll.title)
        assertEquals(2, poll.id)
    }

    //@Test
    fun countByUser() {
        //Given
        val firstUser = createUser(id = 1)
        val secondUser = createUser(id = 2)

        //When
        entityManager.persist(createPoll(id = 1, user = firstUser))

        entityManager.persist(createPoll(id = 2, user = secondUser))
        entityManager.persist(createPoll(id = 2, user = secondUser))
        entityManager.flush()

        assertEquals(1, pollRepository.countByUser(firstUser))
        assertEquals(2, pollRepository.countByUser(secondUser))
    }

    //@Test
    fun deleteByUserAndIdIn() {
        //Given
        val firstUser = createUser(id = 1)
        val secondUser = createUser(id = 2)

        //When
        entityManager.persist(createPoll(id = 1, user = firstUser))
        entityManager.persist(createPoll(id = 2, user = firstUser))
        entityManager.persist(createPoll(id = 3, user = firstUser))

        entityManager.persist(createPoll(id = 4, user = secondUser))
        entityManager.persist(createPoll(id = 5, user = secondUser))
        entityManager.persist(createPoll(id = 6, user = secondUser))
        entityManager.flush()

        val deletedCount = pollRepository.deleteByUserAndIdIn(firstUser,listOf(2,3,4))
        assertEquals(2,deletedCount)
    }

    //@Test
    fun countByUserAndIdIn() {
        //Given
        val firstUser = createUser(id = 1)
        val secondUser = createUser(id = 2)

        //When
        entityManager.persist(createPoll(id = 1, user = firstUser))
        entityManager.persist(createPoll(id = 2, user = firstUser))
        entityManager.persist(createPoll(id = 3, user = firstUser))

        entityManager.persist(createPoll(id = 4, user = secondUser))
        entityManager.persist(createPoll(id = 5, user = secondUser))
        entityManager.persist(createPoll(id = 6, user = secondUser))
        entityManager.flush()

        val firstUserPollsCount = pollRepository.countByUserAndIdIn(firstUser,listOf(2,3,4))
        assertEquals(2,firstUserPollsCount)
    }

    private fun createPoll(id: Long, title: String = "A".repeat(Poll.MIN_LENGTH_TITLE), user: User? = null): Poll {
        val optionTitle = "A".repeat(Option.MIN_LENGTH_TITLE)
        val options = MutableList(Poll.MIN_COUNT_OPTIONS) { Option(id = 0, title = optionTitle, poll = null) }


        if (user != null){
            entityManager.persist(user)
        }

        return Poll(
                id = id,
                title = title,
                slug = title,
                options = options,
                user = user
        )
    }

    private fun createUser(id: Long, email: String = "test@test.com"): User {
        return User(
                id = id,
                email = email,
                password = "123456",
                firstName = "John",
                lastName = "Smith",
                roles = setOf(Role(id = 0, name = Role.USER))
        )
    }
}