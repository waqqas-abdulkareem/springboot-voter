package com.wks.voter.repository

import com.wks.voter.domain.Role
import com.wks.voter.domain.User
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@DataJpaTest
class UserRepositoryTests {

    @Autowired
    private lateinit var entityManager: TestEntityManager

    @Autowired
    private lateinit var userRepository: UserRepository

    @Test
    fun testFindByEmail() {
        //Given
        val testEmail = "test@test.com"
        val user = User(id = 0, email = testEmail, password = "123456", firstName = "John", lastName = "Smith", roles = emptySet<Role>())

        //When
        entityManager.persist(user)
        entityManager.flush()

        //Then
        val result = userRepository.findByEmail(testEmail)!!
        assertEquals(testEmail,result.email)
    }

    @Test
    fun testDuplicateEmail(){
        //Given
        val testEmail = "test@test.com"
        val user = User(id = 0, email = testEmail, password = "123456", firstName = "John", lastName = "Smith", roles = emptySet<Role>())

        //When
        entityManager.persist(user)
        entityManager.persist(user)
        entityManager.flush()

        System.out.println(userRepository.count())
        assertEquals(1,userRepository.count())
    }
}