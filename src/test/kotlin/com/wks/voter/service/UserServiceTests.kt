package com.wks.voter.service

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.wks.voter.domain.Role
import com.wks.voter.domain.User
import com.wks.voter.dto.UserDto
import com.wks.voter.repository.RoleRepository
import com.wks.voter.repository.UserRepository
import com.wks.voter.service.user.DefaultUserService
import com.wks.voter.service.user.UserService
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.verify
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class UserServiceTests {

    private lateinit var userRepository: UserRepository
    private lateinit var userService: UserService

    companion object {
        const val TEST_EMAIL = "test@test.com"
        const val ENCRYPTED_PASSWORD = "Encrypted Password"
    }

    @Before
    fun setup() {
        userRepository = mock<UserRepository>() {
            on { findByEmail(eq(TEST_EMAIL)) } doReturn User(id = 0, email = TEST_EMAIL, password = "123456", firstName = "Test", lastName = "TEST", roles = emptySet())
        }
        val roleRepository = mock<RoleRepository>() {
            on { findByName(eq(Role.USER)) } doReturn Role(id = 0, name = Role.USER)
        }
        val passwordEncoder = mock<BCryptPasswordEncoder>() {
            on { encode(ArgumentMatchers.anyString()) } doReturn ENCRYPTED_PASSWORD
        }
        userService = DefaultUserService(userRepository, roleRepository, passwordEncoder)
    }

    @Test
    fun findUserById() {
        //Given
        val userId = 1L

        //When
        userService.findUserById(userId)

        //Then
        verify(userRepository).findById(eq(userId))
    }

    @Test
    fun findByEmail() {
        //When
        userService.loadUserByUsername(TEST_EMAIL)

        //Then
        verify(userRepository).findByEmail(eq(TEST_EMAIL))
    }

    @Test(expected = UsernameNotFoundException::class)
    fun usernameNotFound() {
        //Given
        val userEmail = "doesnotexist@test.com"

        //When
        userService.loadUserByUsername(userEmail)
    }

    @Test
    fun saveUser() {
        val userCaptor = ArgumentCaptor.forClass<User, User>(User::class.java)

        //Given
        val userDto = UserDto(email = "test@test.com",
                password = "123456",
                firstName = "John",
                lastName = "Smith"
        )

        //When
        userService.saveUser(userDto)

        //Then
        verify(userRepository).save(userCaptor.capture())

        assertEquals(userDto.email, userCaptor.value.email)
        assertEquals(ENCRYPTED_PASSWORD, userCaptor.value.password)
        assertEquals(userDto.firstName, userCaptor.value.firstName)
        assertEquals(userDto.lastName, userCaptor.value.lastName)
        assertEquals(Role.USER, userCaptor.value.roles.first().name)
        assertEquals(true, userCaptor.value.accountEnabled)
        assertEquals(false, userCaptor.value.accountExpired)
        assertEquals(false, userCaptor.value.credentialsExpired)
    }
}