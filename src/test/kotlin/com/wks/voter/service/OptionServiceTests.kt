package com.wks.voter.service

import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.wks.voter.repository.OptionRepository
import com.wks.voter.service.options.DefaultOptionService
import com.wks.voter.service.options.OptionService
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class OptionServiceTests {

    @Mock lateinit var optionsRepository: OptionRepository
    private lateinit var optionService: OptionService

    @Before
    fun setup(){
        optionsRepository = mock()
        optionService = DefaultOptionService(optionsRepository)
    }

    @Test
    fun testOptionById(){
        //Given
        val optionId = 1L

        //When
        optionService.optionById(optionId)

        //Then
        verify(optionsRepository).findById(eq(optionId))
    }
}