package com.wks.voter.service

import com.wks.voter.repository.OptionRepository
import com.wks.voter.repository.PollRepository
import com.wks.voter.service.polls.DefaultPollService
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.test.context.ActiveProfiles

@RunWith(MockitoJUnitRunner::class)
@ActiveProfiles(profiles = ["local"])
class PollServiceTest {

    @Mock lateinit var optionRepository : OptionRepository
    @Mock lateinit var pollRepository: PollRepository

    lateinit var pollService : DefaultPollService

    @Before
    fun setUp(){
        pollService = DefaultPollService(pollRepository, optionRepository)
    }

    @Test
    fun testSlugFromTitle(){
        //Given
        val title = "Who should run for president? <wks@123>"

        //Expected
        val expected = "who_should_run_for_president___wks_123_"
        //val expected = "who should run for president? <wks@123>"


        assertEquals(expected,pollService.slugFromTitle(title))
    }
}