package com.wks.voter.dto

import com.wks.voter.domain.User
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import javax.validation.Validator

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class UserDtoTests {

    @Autowired
    lateinit var validator: Validator
    val testPassword = "A".repeat(User.MIN_LENGTH_PASSWORD)



    @Test
    fun testValidUser() {
        //When
        val userDto = UserDto("test@test.com", testPassword, "John", "Smith")

        val violations = validator.validate(userDto)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testInvalidEmail() {
        //When
        val userDto = UserDto("invalid", testPassword, "John", "Smith")

        val violations = validator.validate(userDto)

        assertEquals(violations.first().propertyPath.toString(), "email")
        assertEquals(violations.first().messageTemplate, "{email.invalid}")
    }

    @Test
    fun testEmptyEmail() {
        //When
        val userDto = UserDto("", testPassword, "John", "Smith")

        val violations = validator.validate(userDto)

        assertEquals(violations.first().propertyPath.toString(), "email")
        assertEquals(violations.first().messageTemplate, "{email.required}")
    }

    @Test
    fun testPasswordTooShort() {
        //When
        val password = "A".repeat( User.MIN_LENGTH_PASSWORD - 1)
        val userDto = UserDto("test@test.com", password, "John", "Smith")

        val violations = validator.validate(userDto)

        assertEquals(violations.first().propertyPath.toString(), "password")
        assertEquals(violations.first().messageTemplate, "{password.length}")
    }

    @Test
    fun testPasswordMinLength() {
        //When
        val password = "A".repeat(User.MIN_LENGTH_PASSWORD)
        val userDto = UserDto("test@test.com", password, "John", "Smith")

        val violations = validator.validate(userDto)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testPasswordTooLong() {
        //When
        val password = "A".repeat(User.MAX_LENGTH_PASSWORD + 1)
        val userDto = UserDto("test@test.com", password, "John", "Smith")

        val violations = validator.validate(userDto)

        assertEquals(violations.first().propertyPath.toString(), "password")
        assertEquals(violations.first().messageTemplate, "{password.length}")
    }

    @Test
    fun testPasswordMaxLength() {
        //When
        val password = "A".repeat(User.MAX_LENGTH_PASSWORD)
        val userDto = UserDto("test@test.com", password, "John", "Smith")

        val violations = validator.validate(userDto)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testFirstNameEmpty() {
        //When
        val userDto = UserDto("test@test.com", testPassword, "", "Smith")

        val violations = validator.validate(userDto)

        assertEquals(violations.first().propertyPath.toString(), "firstName")
        assertEquals(violations.first().messageTemplate, "{first_name.required}")
    }

    @Test
    fun testLastNameEmpty() {
        //When
        val userDto = UserDto("test@test.com", testPassword, "John", "")

        val violations = validator.validate(userDto)

        assertEquals(violations.first().propertyPath.toString(), "lastName")
        assertEquals(violations.first().messageTemplate, "{last_name.required}")
    }
}