package com.wks.voter.dto

import com.wks.voter.domain.Option
import com.wks.voter.domain.Poll
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import javax.validation.Validator

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class PollDtoTests {

    @Autowired
    lateinit var validator: Validator

    private val validTitle = "A".repeat(Poll.MIN_LENGTH_TITLE)
    private val validOptionTitle = "A".repeat(Option.MIN_LENGTH_TITLE)
    private val validOptions = List(Poll.MIN_COUNT_OPTIONS) { validOptionTitle }

    @Test
    fun testValidPoll() {
        val poll = PollDto(validTitle, validOptions)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTitleTooShort() {
        val title = "A".repeat(Poll.MIN_LENGTH_TITLE - 1)
        val poll = PollDto("W", validOptions)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "title")
        assertEquals(violations.first().messageTemplate, "{poll.title.length}")
    }

    @Test
    fun testTitleMinLength() {
        val title = "A".repeat(Poll.MIN_LENGTH_TITLE)
        val poll = PollDto(title, validOptions)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTitleMaxLength() {
        val title = "A".repeat(Poll.MAX_LENGTH_TITLE)

        val poll = PollDto(title, validOptions)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTitleTooLong() {
        val title = "A".repeat(Poll.MAX_LENGTH_TITLE + 1)

        val poll = PollDto(title, validOptions)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "title")
        assertEquals(violations.first().messageTemplate, "{poll.title.length}")
    }

    @Test
    fun testTitleEmpty() {
        val poll = PollDto("", validOptions)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "title")
        assertEquals(violations.first().messageTemplate, "{poll.title.length}")
    }

    @Test
    fun testTooFewOptions() {
        val options = List(Poll.MIN_COUNT_OPTIONS - 1, { validOptionTitle })
        val poll = PollDto(validTitle, options)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "options")
    }

    @Test
    fun testMinimumOptions() {
        val options = List(Poll.MIN_COUNT_OPTIONS, { validOptionTitle })
        val poll = PollDto(validTitle, options)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testTooManyOptions() {
        val options = List(Poll.MAX_COUNT_OPTIONS + 1, { validOptionTitle })
        val poll = PollDto(validTitle, options)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "options")
    }

    @Test
    fun testMaximumOptions() {
        val options = List(Poll.MAX_COUNT_OPTIONS, { validOptionTitle })
        val poll = PollDto(validTitle, options)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }

    @Test
    fun testInvalidOptions() {
        val options = List(Poll.MIN_COUNT_OPTIONS, { "A".repeat(Option.MIN_LENGTH_TITLE - 1) })
        val poll = PollDto(validTitle, options)

        val violations = validator.validate(poll)

        assertEquals(violations.first().propertyPath.toString(), "options")
    }

    @Test
    fun testValidOptions() {
        val options = MutableList(Poll.MIN_COUNT_OPTIONS, { validOptionTitle })
        val poll = PollDto(validTitle, options)

        val violations = validator.validate(poll)

        assertTrue(violations.isEmpty())
    }
}