package com.wks.voter.dto

import com.wks.voter.domain.Option
import com.wks.voter.domain.Poll
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import java.time.LocalDateTime

@RunWith(SpringRunner::class)
@ActiveProfiles(profiles = ["local"])
@SpringBootTest
class PollResultsDtoTests {

    @Test
    fun testConstructor() {

        val options = listOf(
                Option(id = 1, title = "Chocolate", poll = null),
                Option(id = 2, title = "Vanilla", poll = null),
                Option(id = 3, title = "Strawberry", poll = null)
        )
        val poll = Poll(id = 1,
                title = "What is your favourite flavour?",
                slug = "what_is_your_favourite_flavour_",
                enabled = true,
                createdDate = LocalDateTime.now(),
                modifiedDate = LocalDateTime.now(),
                options = options.toMutableList(),
                user = null
        )

        val rawResults = listOf(
                OptionIdVotesDto(options.first().id, 1),
                OptionIdVotesDto(options.last().id, 0)
        )

        val pollResultsDto = PollResultsDto(poll = poll, rawResults = rawResults)

        assertEquals(pollResultsDto.id, poll.id)
        assertEquals(pollResultsDto.title, poll.title)
        assertEquals(pollResultsDto.slug, poll.slug)
        assertEquals(pollResultsDto.totalVotes, rawResults.map { it.votes }.sum())
        assertEquals(pollResultsDto.options.first().title, options.first().title)
        assertEquals(pollResultsDto.options.first().votes, rawResults.first().votes)
        assertEquals(pollResultsDto.options.last().title, options.last().title)
        assertEquals(pollResultsDto.options.last().votes, rawResults.last().votes)
    }
}