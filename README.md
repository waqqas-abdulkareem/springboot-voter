#Voter

## Project Description:

[Description](https://www.freecodecamp.org/challenges/build-a-voting-app)

[Inspiration](http://votingapp.herokuapp.com/)

## To Do

1) Account(`/login`, `/logout`, `/register`)

- [x] Login
- [x] Update Navigation Bar to show Log-out after logging-in
- [x] Sign-up Form
- [x] Sign-up form client side validation
- [x] Sign-up form server side validation
- [x] Account creation
- [x] Show sign up, home, login as active
- [x] Show Hello item once logged in.
- [x] use UserDetails service so that principal can be cast to User

Ignore: Forget, Change password, etc...

2) `/`

- [x] Display list of recent polls
- [x] link to all polls by a particular user.

3) `/poll` PollController

- [x] Only available for signed-in user
- [x] Form to create new poll with options
- [x] Once form is created, show url of poll
- [x] Create slug on backend
- [x] Show error if all options are empty or there are no options (used d-block bootstrap)
- [x] ensure no more than 5 options can be provided for using custom validator on model

4) `/{user}/polls/{slug}` PollsController

- [x] Show title and options of poll
- [x] submit vote result
- [x] If user accesses this page, he is redirected to results
- [x] handle user not found exception

5) `/{user}/polls`

- [x] List of polls of a user
- [x] If user accesses this page, he can click on them to edit
- [x] Fix issue: css not applied
- [x] User can select and delete polls in bulk
- [x] show poll owner name in title (bug)
- [x] Confirm deleting polls
- [x] Show error if polls could not be deleted or if there is an error

6) `/{user}/polls/{slug}/result`

- [x] Shows graphical result of poll
- [x] Show title and poll owner name
- [x] Centre chart
- [x] Show total votes
- [x] Use different colour to indicate user's selection

7)

- [x] Global Error handling
- [-] Unit Tests
- [x] Full localization
- [X] Set up Logging
- [x] Set up actuator
- [x] Do not let actuator httptrace endpoint log resource requests (js/css)
- [x] Require admin access to view actuator
- [x] Is there a way to pass data while redirecting to prevent repeating queries?
- [ ] Fix Repetition in VoteController

8)

- [x] Docker, passing in dev/prod profiles
- [x] Bug: Error message not shown when password too long
- [ ] Bug: Vote options validator should show separate message for each validation
- [x] Bug: Poll with 5 options only shows the first two
- [x] Bug: Can't delete polls on Docker (db issue)
- [x] Testing localization
- [ ] Kubernetes?
- [x] RDS Mysql Database on AWS
- [ ] HTTPS Configuration
- [ ] Hosting via docker on AWS

9) Last Minute Feature - Profile Picture

- [x] Upload profile picture
- [x] View Profile Picture
- [x] Profile Picture Error handling
- [ ] Bug: Update profile picture immediately after upload (requires re login atm)
- [x] Test Profile Picture from docker container
- [x] S3 for storage

## Structures

```
User:
- [polls]

Poll:
- id
- user_email (required)
- title (required,max 128)
- slug (required,unique,max 64, non-editable)
- [option](required,max10)
- enabled  (boolean)

Option:
- id
- title (required, max 128)

Vote:
- id
- user_id
- poll_id
- option_id
UNIQUE user_id,poll_id
```

## Reference Material:

1. [Setting up login/sign-up using Spring Boot](https://medium.com/@gustavo.ponce.ch/spring-boot-spring-mvc-spring-security-mysql-a5d8545d837d)
2. [Validation message resources in Kotlin](https://stonesoupprogramming.com/2017/06/21/spring-bean-validation-example-jsr-303-in-kotlin/)
3. [Authentication with a Database-backed UserDetailsService](http://www.baeldung.com/spring-security-authentication-with-a-database)
4. [Unit Tests](https://medium.com/@rhwolniewicz/building-a-rest-service-with-kotlin-spring-boot-mongodb-and-junit-14d10faa594b)
5. [Passing Data while redirecting (RequestAttributes)](http://www.baeldung.com/spring-redirect-and-forward)
6. [Baeldung's guide to actuators](http://www.baeldung.com/spring-boot-actuators)
7. [Spring Boot Actuator Documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html)
8. [Spring Boot 2 Migration Guide](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.0-Migration-Guide)
9. [Customizing HTTPTrace actuator endpoint](http://www.sedooe.com/2017/08/tracing-requests-and-responses-with-spring-boot-actuator/)
10. [Setting up schema in mysql docker container](https://stackoverflow.com/questions/36617682/docker-compose-mysql-import-sql)
11. [Setting up S3 with AWS SDK](https://devcenter.heroku.com/articles/using-amazon-s3-for-file-uploads-with-java-and-play-2)